Python Code Integration Task
=================================

## Intro

Let's assume you are already working with some colleagues on a Machine Learning related project. The very basic task is to load images, so they are available for a number of different tasks.

Your colleague has drafted an initial loader and has asked for help to ensure it can be shared and used by other colleagues and to improve the code quality. Quality here refers to good practices in Python and in the repo itself. We can assume that, in the future, multiple tasks will be offered through the same codebase to which this dataloader belongs.

Again, the emphasis here will be on overall code and repo quality.

Although improvements can be endless, please do not spend more than 3 hours on this task. You are completely free to choose what you improve on.

## Tasks

* Improve repo and code quality.
* Ensure code can be easily extended, shared and run by any colleague.

For development purposes, sample data has been provided. Notice it is only a subset of a much bigger dataset. Feel free to use it or other non-proprietary data of your choice.

## Deliverable

Feel free to share your work however you prefer.
At the interview a working python environment capable of running this code is also expected.
