import os
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from torchvision.transforms import functional as F
import pandas as pd
import numpy as np
import cv2, torch, json

def ToNormalizedTensor(image):
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    H,W = image.shape[0:2]
    image = image[:,:,::-1] # opencv loading is BGR, so convert it to RGB
    image = (image.astype('float32')/255.0 - mean)/std
    image = np.rollaxis(image, 2, 0)
    return image


class GTSRB(Dataset):
    def __init__(self, mode, root = None, transform = None):
        self.mode = mode


        assert mode in ['train', 'test']

        if root is not None:
            self.root = root
        else:
            if mode == 'train':
                self.root = '/storage.spa.siemens.com/dlf.public/gtsrb/Final_Training/Images/'
            else:
                self.root = '/storage.spa.siemens.com/dlf.public/gtsrb/Final_Test/Images/'

        self.transform = transform


    def load_train_data(self):
        dirs = os.listdir(self.root)
        objects = []
        for _dir in dirs:
            __dir = os.path.join(self.root, _dir)
            files = os.listdir(__dir)
            csv = [f for f in files if f.endswith('csv')][0]
            csv_url = os.path.join(__dir, csv)
            images = [f for f in files if f.endswith('ppm')]
            df = self.load_csv(csv_url)
            ClassId = df['ClassId'].values[0]
            for image in images:
                cls = {
                    'dir': _dir,
                    'fulldir': __dir,
                    'image': image,
                    'ClassId': ClassId
                }
                objects.append(cls)
        self.objects = objects
        self.N = len(objects)


    def load_test_data(self):

        objects = []

        __dir = self.root
        files = os.listdir(__dir)
        csv = [f for f in files if f.endswith('csv')][0]
        csv_url = os.path.join(__dir, csv)
        images = [f for f in files if f.endswith('ppm')]
        df = self.load_csv(csv_url)
        ClassId = df['ClassId'].values[0]
        for image in images:
            cls = {
                'dir': __dir,
                'fulldir': __dir,
                'image': image,
                'ClassId': ClassId
            }
            objects.append(cls)
        self.objects = objects
        self.N = len(objects)


    def load_dataset(self):
        if self.mode == 'train':
            self.load_train_data()
        else:
            self.load_test_data()
        print (f'Loaded {self.N} objects for mode {self.mode}')


    def load_csv(self, csv_url):
        df = pd.read_csv(csv_url, delimiter=';')
        return df

    def fill_classes(self):
        for obj in self.objects:
            csv = self.load_csv(obj)
            obj['df'] = csv
            obj['ClassId'] = csv['ClassId'].values[0]

    def __len__(self):
        return self.N

    def __getitem__(self, index):
        item = self.objects[index]
        url = os.path.join(item['fulldir'], item['image'])
        image = cv2.imread(url)
        classid = item['ClassId']
        image_ = mixed = torch.from_numpy(self.transform(image).copy())

        return  image_, classid
